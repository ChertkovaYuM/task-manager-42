package ru.tsc.chertkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Role;

import javax.persistence.Entity;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
public final class Session extends AbstractUserOwnerModel {

    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;

}
