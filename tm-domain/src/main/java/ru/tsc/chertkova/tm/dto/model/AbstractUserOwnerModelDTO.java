package ru.tsc.chertkova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.model.IWBS;
import ru.tsc.chertkova.tm.dto.model.AbstractModelDTO;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnerModelDTO extends AbstractModelDTO implements IWBS {

    @Nullable
    @Column(name = "USER_ID")
    private String userId;

}
