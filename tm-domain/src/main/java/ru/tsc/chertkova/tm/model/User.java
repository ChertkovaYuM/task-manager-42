package ru.tsc.chertkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
public class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = "email")
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "locked")
    private boolean locked = false;

    @NotNull
    @Column(name = "login")
    private String login;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(name = "password")
    private String passwordHash;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role = Role.USUAL;

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Task> tasks = new ArrayList<>();

    public User(@NotNull final String login, @NotNull final String passwordHash,
                @NotNull final String email, @Nullable final String firstName,
                @Nullable final String middleName, @Nullable final String lastName) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Id " + getId() +
                " email='" + email +
                ", firstName='" + firstName +
                ", lastName='" + lastName +
                ", middleName='" + middleName +
                ", role=" + role;
    }

    public boolean getLocked() {
        return locked;
    }

}
