package ru.tsc.chertkova.tm.util;

import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    static Date nextDate() {
        @NotNull final String value = nextLine();
        return DateUtil.toDate(value);
    }

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @NotNull final String value = SCANNER.nextLine();
        if (value.matches("\\d*")) return Integer.parseInt(value);
        else return 0;
    }

}
