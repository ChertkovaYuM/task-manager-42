package ru.tsc.chertkova.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class CalculatorEndpoint {

    @WebMethod
    public int sum(@WebParam(name = "a") int a,
                   @WebParam(name = "b") int b) {
        return a + b;
    }

}
