package ru.tsc.chertkova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.Project;

import java.sql.Timestamp;
import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO public.\"tm_project\" " +
            "(\"id\",\"user_id\",\"name\",\"description\",\"status\"," +
            "\"created_dt\"," +
            "\"started_dt\",\"completed_dt\")" +
            " VALUES (#{id},#{userId},#{name},#{description}," +
            "#{status},#{created},#{dateBegin},#{dateEnd})")
    void add(@Param("id") @NotNull String id, @Param("userId") @NotNull String userId,
             @Param("name") @NotNull String name, @Param("description") @NotNull String description,
             @Param("status") @NotNull String status, @Param("created") @NotNull Timestamp created,
             @Param("dateBegin") @NotNull Timestamp dateBegin, @Param("dateEnd") @NotNull Timestamp dateEnd);

    @Delete("DELETE FROM public.\"tm_project\" WHERE \"user_id\"=#{userId}")
    void clear(@Param("userId") @NotNull String userId);

    @Nullable
    @Select("SELECT * FROM public.\"tm_project\" WHERE \"user_id\"=#{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateBegin", column = "started_dt"),
            @Result(property = "dateEnd", column = "completed_dt"),
            @Result(property = "userId", column = "user_id")
    })
    List<Project> findAll(@Param("userId") @NotNull String userId);

    @Nullable
    @Select("SELECT * FROM public.\"tm_project\" WHERE \"user_id\"=#{userId} AND \"id\"=#{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateBegin", column = "started_dt"),
            @Result(property = "dateEnd", column = "completed_dt"),
            @Result(property = "userId", column = "user_id")
    })
    Project findById(@Param("userId") @NotNull String userId,
                     @Param("id") @NotNull String id);

    @Select("SELECT COUNT(1) FROM public.\"tm_project\" WHERE \"user_id\"=#{userId}")
    int getSize(@Param("userId") @NotNull String userId);

    @Delete("DELETE FROM public.\"tm_project\" WHERE \"id\"=#{id} LIMIT 1")
    void removeById(@Param("userId") @NotNull String userId,
                    @Param("id") @NotNull String id);

    @Update("UPDATE public.\"tm_project\" " +
            "SET \"name\"=#{name} AND \"description\"=#{description} " +
            "WHERE \"id\" = #{id} AND \"user_id\"=#{userId}")
    void update(@Param("id") @NotNull String id,
                @Param("userId") @NotNull String userId,
                @Param("name") @NotNull String name,
                @Param("description") @NotNull String description);

    @Update("UPDATE public.\"tm_project\" " +
            "SET \"status\"=#{status} " +
            "WHERE \"id\" = #{id} AND \"user_id\"=#{userId}")
    void changeStatus(@Param("id") @NotNull String id,
                      @Param("userId") @NotNull String userId,
                      @Param("status") @NotNull String status);

    @Select("SELECT COUNT(1) FROM public.\"tm_project\" WHERE \"id\"=#{id}")
    int existsById(@Param("id") @Nullable String id);

}
