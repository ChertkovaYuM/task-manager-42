package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IUserOwnerService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.model.AbstractUserOwnerModel;

public abstract class AbstractUserOwnerService<M extends AbstractUserOwnerModel>
        extends AbstractService<M> implements IUserOwnerService<M> {

    @Nullable
    protected final IUserService userService;

    public AbstractUserOwnerService(@NotNull IConnectionService connectionService, @Nullable IUserService userService) {
        super(connectionService);
        this.userService = userService;
    }

}
