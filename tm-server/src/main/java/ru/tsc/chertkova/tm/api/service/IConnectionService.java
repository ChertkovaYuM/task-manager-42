package ru.tsc.chertkova.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    SqlSessionFactory getSqlSessionFactory();

    @NotNull
    SqlSession getSqlSession();

    @NotNull
    EntityManagerFactory factory();

}
