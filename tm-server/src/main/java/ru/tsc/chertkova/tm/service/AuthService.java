package ru.tsc.chertkova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.service.IAuthService;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.exception.entity.UserNotFoundException;
import ru.tsc.chertkova.tm.exception.user.LoginEmptyException;
import ru.tsc.chertkova.tm.exception.user.PasswordEmptyException;
import ru.tsc.chertkova.tm.exception.user.AccessDeniedException;
import ru.tsc.chertkova.tm.model.Session;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.util.CryptUtil;
import ru.tsc.chertkova.tm.util.HashUtil;

import java.util.Date;

public class AuthService implements IAuthService {

    @Nullable
    private final IPropertyService propertyService;

    @Nullable
    private final IUserService userService;

    public AuthService(@Nullable final IPropertyService propertyService,
                       @Nullable final IUserService userService) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session validateToken(@Nullable String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @Nullable String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull Session session = objectMapper.readValue(json, Session.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = ((currentDate.getTime() - sessionDate.getTime()) / 1000);
        if (delta > propertyService.getSessionTimeout()) throw new AccessDeniedException();
        return session;
    }

    @Override
    @Nullable
    public User registry(@Nullable final String login,
                         @Nullable final String password,
                         @Nullable final String email) {
        return userService.add(new User(login, password, email, null, null, null));
    }

    @NotNull
    @Override
    public String login(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (user.getLocked()) throw new UserNotFoundException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return getToken(user);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final User user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final Session session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    private Session createSession(User user) {
        @NotNull Session session = new Session();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return session;
    }

}
