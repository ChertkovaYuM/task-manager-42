package ru.tsc.chertkova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.model.User;

import java.sql.Timestamp;
import java.util.List;

public interface IUserRepository {

    @Nullable
    @Select("SELECT * FROM public.\"tm_user\" WHERE \"login\"=#{login} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "email", column = "email"),
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "role", column = "role"),
            @Result(property = "locked", column = "locked")
    })
    User findByLogin(@Param("login") @NotNull String login);

    @Nullable
    @Select("SELECT * FROM public.\"tm_user\" WHERE \"email\"=#{email} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "email", column = "email"),
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "role", column = "role"),
            @Result(property = "locked", column = "locked")
    })
    User findByEmail(@Param("email") @NotNull String email);

    @Delete("DELETE FROM public.\"tm_user\" WHERE \"login\"=#{login} LIMIT 1")
    void removeByLogin(@Param("login") @NotNull String login);

    @Select("SELECT COUNT(1) FROM public.\"tm_user\" WHERE \"login\"=#{login} LIMIT 1")
    int isLoginExist(@Param("login") @NotNull String login);

    @Select("SELECT COUNT(1) FROM public.\"tm_user\" WHERE \"email\"=#{email} LIMIT 1")
    int isEmailExist(@Param("email") @NotNull String email);

    @Insert("INSERT INTO public.\"tm_user\" " +
            "(\"id\",\"login\",\"first_name\"," +
            "\"middle_name\",\"last_name\"," +
            "\"created_dt\",\"email\",\"password\"," +
            "\"role\",\"locked\")" +
            " VALUES (#{id},#{login},#{firstName},#{middleName}," +
            "#{lastName},#{created_dt}," +
            "#{email},#{password},#{role},#{locked})")
    void add(@Param("id") @NotNull String id, @Param("login") @NotNull String login,
             @Param("firstName") @Nullable String firstName, @Param("middleName") @Nullable String middleName,
             @Param("lastName") @Nullable String lastName, @Param("created_dt") @NotNull Timestamp created,
             @Param("email") @NotNull String email, @Param("password") @NotNull String password,
             @Param("role") @Nullable Role role, @Param("locked") @NotNull Boolean locked);

    @Delete("DELETE FROM public.\"tm_user\"")
    void clear();

    @Nullable
    @Select("SELECT * FROM public.\"tm_user\"")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "email", column = "email"),
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "role", column = "role"),
            @Result(property = "locked", column = "locked")
    })
    List<User> findAll();

    @Nullable
    @Select("SELECT * FROM public.\"tm_user\" WHERE \"id\"=#{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "email", column = "email"),
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "role", column = "role"),
            @Result(property = "locked", column = "locked")
    })
    User findById(@Param("id") @NotNull String id);

    @Select("SELECT COUNT(1) FROM public.\"tm_user\"")
    int getSize();

    @Delete("DELETE FROM public.\"tm_user\" WHERE \"id\"=#{id} LIMIT 1")
    void removeById(@Param("id") @NotNull String id);

    @Update("UPDATE public.\"tm_user\" " +
            "SET \"first_name\"=#{firstName} AND \"middle_name\"=#{middleName} " +
            "AND \"last_name\"=#{lastName} " +
            "WHERE \"id\" = #{id}")
    void update(@Param("id") @NotNull String id,
                @Param("firstName") @NotNull String firstName,
                @Param("middleName") @NotNull String middleName,
                @Param("lastName") @NotNull String lastName);

    @Update("UPDATE public.\"tm_user\" " +
            "SET \"role\"=#{role} " +
            "WHERE \"id\" = #{id}")
    void changeRole(@Param("id") @NotNull String id,
                    @Param("role") @NotNull Role role);

    @Select("SELECT COUNT(1) FROM public.\"tm_user\" WHERE \"id\"=#{id}")
    int existsById(@Param("id") @NotNull String id);

    @Update("UPDATE public.\"tm_user\" " +
            "SET \"password\"=#{password} " +
            "WHERE \"id\" = #{id}")
    void setPassword(@Param("id") @NotNull String id,
                     @Param("password") @NotNull String passwordHash);

    @Update("UPDATE public.\"tm_user\" " +
            "SET \"locked\"=#{locked} " +
            "WHERE \"login\" = #{login}")
    void setLockedFlag(@Param("login") @NotNull String login,
                       @Param("locked") @NotNull Boolean locked);

}
