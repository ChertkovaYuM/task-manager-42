package ru.tsc.chertkova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.model.Task;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    @Nullable
    @Select("SELECT * FROM public.\"tm_task\" WHERE \"user_id\"=#{userId} AND \"project_id\"=#{projectId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateBegin", column = "started_dt"),
            @Result(property = "dateEnd", column = "completed_dt"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllByProjectId(@Param("userId") @Nullable String userId,
                                  @Param("projectId") @Nullable String projectId);

    @Insert("INSERT INTO public.\"tm_task\" " +
            "(\"id\",\"user_id\",\"name\",\"description\",\"status\"," +
            "\"created_dt\"," +
            "\"started_dt\",\"completed_dt\",\"project_id\")" +
            " VALUES (#{id},#{userId},#{name},#{description}," +
            "#{status},#{created},#{dateBegin},#{dateEnd},#{projectId})")
    void add(@Param("id") @NotNull String id, @Param("userId") @NotNull String userId,
             @Param("name") @NotNull String name, @Param("description") @NotNull String description,
             @Param("status") @NotNull String status, @Param("created") @NotNull Timestamp created,
             @Param("dateBegin") @NotNull Timestamp dateBegin, @Param("dateEnd") @NotNull Timestamp dateEnd,
             @Param("projectId") @NotNull String projectId);

    @Delete("DELETE FROM public.\"tm_task\" WHERE \"user_id\"=#{userId}")
    void clear(@Param("userId") @NotNull String userId);

    @Nullable
    @Select("SELECT * FROM public.\"tm_task\" WHERE \"user_id\"=#{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateBegin", column = "started_dt"),
            @Result(property = "dateEnd", column = "completed_dt"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAll(@Param("userId") @NotNull String userId);

    @Nullable
    @Select("SELECT * FROM public.\"tm_task\" WHERE \"user_id\"=#{userId} AND \"id\"=#{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateBegin", column = "started_dt"),
            @Result(property = "dateEnd", column = "completed_dt"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findById(@Param("userId") @NotNull String userId,
                  @Param("id") @NotNull String id);

    @Select("SELECT COUNT(1) FROM public.\"tm_task\" WHERE \"user_id\"=#{userId}")
    int getSize(@Param("userId") @NotNull String userId);

    @Delete("DELETE FROM public.\"tm_task\" WHERE \"id\"=#{id} LIMIT 1")
    void removeById(@Param("userId") @NotNull String userId,
                    @Param("id") @NotNull String id);

    @Update("UPDATE public.\"tm_task\" " +
            "SET \"name\"=#{name} AND \"description\"=#{description} " +
            "WHERE \"id\" = #{id} AND \"user_id\"=#{userId}")
    void update(@Param("id") @NotNull String id,
                @Param("userId") @NotNull String userId,
                @Param("name") @NotNull String name,
                @Param("description") @NotNull String description);

    @Update("UPDATE public.\"tm_task\" " +
            "SET \"status\"=#{status} " +
            "WHERE \"id\" = #{id} AND \"user_id\"=#{userId}")
    void changeStatus(@Param("id") @NotNull String id,
                      @Param("userId") @NotNull String userId,
                      @Param("status") @NotNull String status);

    @Select("SELECT COUNT(1) FROM public.\"tm_task\" WHERE \"id\"=#{id}")
    int existsById(@Param("id") @Nullable String id);

    @Update("UPDATE public.\"tm_task\" " +
            "SET \"project_id\"=#{projectId} " +
            "WHERE \"id\" = #{taskId} AND \"user_id\"=#{userId}")
    void bindTaskToProject(@Param("taskId") @NotNull String taskId,
                           @Param("projectId") @NotNull String projectId,
                           @Param("userId") @NotNull String userId);

}
