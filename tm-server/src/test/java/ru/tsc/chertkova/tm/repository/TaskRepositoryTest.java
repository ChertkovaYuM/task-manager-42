package ru.tsc.chertkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.chertkova.tm.api.repository.ITaskRepository;
import ru.tsc.chertkova.tm.marker.UnitCategory;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.service.ConnectionService;
import ru.tsc.chertkova.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;

import static ru.tsc.chertkova.tm.constant.TaskTestData.*;
import static ru.tsc.chertkova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public class TaskRepositoryTest {
//
//    @NotNull
//    private final IPropertyService propertyService = new PropertyService();
//
//    @NotNull
//    private final IConnectionService connectionService = new ConnectionService(propertyService);
//
//    @NotNull
//    private final ITaskRepository repository = new TaskRepository(connectionService.getConnection());
//
//    @Test
//    public void add() {
//        Assert.assertTrue(repository.findAll().isEmpty());
//        repository.add(USER1_TASK1);
//        Assert.assertEquals(USER1_TASK1, repository.findAll().get(0));
//    }
//
//    @Test
//    public void addByUserId() {
//        Assert.assertTrue(repository.findAll().isEmpty());
//        repository.add(USER1.getId(), USER1_TASK1);
//        Assert.assertEquals(USER1_TASK1, repository.findAll().get(0));
//    }
//
//    @Test
//    public void addAll() {
//        Assert.assertTrue(repository.findAll().isEmpty());
//        addAll(repository, TASK_LIST);
//        Assert.assertEquals(TASK_LIST, repository.findAll());
//    }
//
//    private void addAll(ITaskRepository repository, List<Task> tasks) {
//        for (Task t :
//                tasks) {
//            repository.add(t);
//        }
//    }
//
//    @Test
//    public void remove() {
//        @NotNull final List<Task> list = new ArrayList<>(USER1_TASK_LIST);
//        addAll(repository, USER1_TASK_LIST);
//        //@Nullable final Task removed = repository.remove(USER1_TASK2);
//        //Assert.assertEquals(USER1_TASK2, removed);
//        list.remove(USER1_TASK2);
//        Assert.assertEquals(list, repository.findAll());
//        //Assert.assertNull(repository.remove(USER1_TASK2));
//    }
//
//    @Test
//    public void removeByUserId() {
//        @NotNull final List<Task> list = new ArrayList<>(TASK_LIST);
//        addAll(repository, TASK_LIST);
//        repository.remove(USER1.getId(), USER1_TASK2);
//        list.remove(USER1_TASK2);
//        Assert.assertEquals(list, repository.findAll());
//
//        repository.remove(USER1.getId(), ADMIN1_TASK1);
//        Assert.assertTrue(TASK_LIST.contains(ADMIN1_TASK1));
//        list.remove(ADMIN1_TASK1);
//        Assert.assertNotEquals(list, repository.findAll());
//    }
//
//    @Test
//    public void clear() {
//        addAll(repository, TASK_LIST);
//        repository.clear();
//        Assert.assertTrue(repository.findAll().isEmpty());
//    }
//
//    @Test
//    public void clearByUserId() {
//        addAll(repository, TASK_LIST);
//        repository.clear(USER1.getId());
//        Assert.assertFalse(repository.findAll().containsAll(USER1_TASK_LIST));
//        @NotNull final List<Task> result = new ArrayList<>(TASK_LIST);
//        result.removeAll(USER1_TASK_LIST);
//        Assert.assertEquals(repository.findAll(), result);
//    }
//
//    @Test
//    public void findAll() {
//        Assert.assertTrue(repository.findAll().isEmpty());
//        addAll(repository, TASK_LIST);
//        Assert.assertEquals(TASK_LIST, repository.findAll());
//    }
//
//    @Test
//    public void findAllByUserId() {
//        Assert.assertTrue(repository.findAll().isEmpty());
//        addAll(repository, TASK_LIST);
//        Assert.assertEquals(ADMIN1_TASK_LIST, repository.findAll(ADMIN1.getId()));
//    }
//
//    @Test
//    public void findById() {
//        Assert.assertTrue(repository.findAll().isEmpty());
//        addAll(repository, TASK_LIST);
//        Assert.assertEquals(
//                repository.findById(USER1.getId(), USER1_TASK2.getId()),
//                USER1_TASK2
//        );
//        Assert.assertNull(repository.findById(USER1.getId(), ADMIN1_TASK2.getId()));
//    }
//
//    @Test
//    public void removeById() {
//        Assert.assertTrue(repository.findAll().isEmpty());
//        addAll(repository, TASK_LIST);
//        Assert.assertEquals(
//                repository.removeById(ADMIN1.getId(), ADMIN1_TASK2.getId()),
//                ADMIN1_TASK2
//        );
//        @NotNull final List<Task> list = new ArrayList<>(TASK_LIST);
//        list.remove(ADMIN1_TASK2);
//        Assert.assertEquals(repository.findAll(), list);
//        Assert.assertNull(repository.removeById(USER1.getId(), ADMIN1_TASK2.getId()));
//        Assert.assertEquals(repository.findAll(), list);
//    }

}
