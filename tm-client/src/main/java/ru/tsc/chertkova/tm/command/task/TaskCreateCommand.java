package ru.tsc.chertkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.task.TaskCreateRequest;
import ru.tsc.chertkova.tm.util.TerminalUtil;

import java.util.Date;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-create";

    @NotNull
    public static final String DESCRIPTION = "Create new task.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        //System.out.println("ENTER DATE BEGIN:");
        //@Nullable final Date dateBegin = TerminalUtil.nextDate();
        //System.out.println("ENTER DATE END:");
        //@Nullable final Date dateEnd = TerminalUtil.nextDate();
        getServiceLocator().getTaskEndpoint().createTask(new TaskCreateRequest(getToken(), name, description, projectId));
    }

}
