package ru.tsc.chertkova.tm.command.domain;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.Domain;
import ru.tsc.chertkova.tm.dto.request.data.DataXmlFasterXmlLoadRequest;
import ru.tsc.chertkova.tm.enumerated.Role;

import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DomainXmlFasterXmlLoadCommand extends AbstractDomainCommand {

    @NotNull
    public static final String NAME = "data-load-xml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Load date from xml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        getServiceLocator().getDomainEndpoint().loadDataXmlFasterXml(new DataXmlFasterXmlLoadRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
